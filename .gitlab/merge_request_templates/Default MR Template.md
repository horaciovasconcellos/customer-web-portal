## Why is this change being made?

<!--
Provide a detailed answer to question on **why** this change is being proposed, in accordance with our value of [Transparency][transparency].

Example: `We have discussed the topic in Slack - (copy of Slack conversation). The current process is not efficient, this MR makes the description of X more clear, and helps move Y forward.`
-->

## Author Checklist

<!-- Please verify the check list and ensure to tick them off before the MR is merged. -->

- [ ] Provided a concise title for the MR
- [ ] Added a description to this MR explaining the reasons for the proposed change, per [say-why-not-just-what][transparency]
  - Copy/paste the Slack conversation to document it for later, or upload screenshots. Verify that no confidential data is added.
- [ ] Assign reviewers for this change to the correct DRI(s)
    - If the DRI for the page/s being updated isn’t immediately clear, then assign it to one of the people listed in the "Maintained by" section in on the page being edited.
    - If your manager does not have merge rights, please ask someone to merge it AFTER it has been approved by your manager in [#mr-buddies][mr-buddies-slack].
- [ ] If the changes affect team members, or warrant an announcement in another way, please consider posting an update in [#whats-happening-at-gitlab][whats-happening-at-gitlab-slack] linking to this MR.
  - If this is a change that directly impacts the majority of global team members, it should be a candidate for [#company-fyi][company-fyi-slack]. Please work with [internal communications][internal-communications] and check the handbook for examples. 

